from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#function used to set the starting pose in the botton corner for the M and F letters
def reset_to_start_pose(move_group):
    #assign start position joint goal in bottom left
    joint_goal = [0.9748608933070226, -0.8973703529804808, 1.663464276618793, -0.7659386924442249, 0.9742355148999673, 0.013703847547324344]
    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    move_group.go(joint_goal, wait=True)
    # Calling ``stop()`` ensures that there is no residual movement
    move_group.stop()

def execute_trajectory(move_group, waypoints):
    #calculate cartesian plan
    (plan, fraction) = move_group.compute_cartesian_path(
        waypoints, 0.01, 0.0)

    #execute plan to draw letter
    move_group.execute(plan, wait=True)
    

#initialize robot and scene
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("ur5", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

#create move_group
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)


display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#move EE to start position in bottom lift
reset_to_start_pose(move_group)

#use waypoints to plot path of letter M
scale = 1.0
waypoints = []

#get current pose
wpose = move_group.get_current_pose().pose

wpose.position.x += scale * 0.4  # draw first line of M
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.3
wpose.position.y -= scale * 0.15  # draw first diagonal down
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.3
wpose.position.y -= scale * 0.15  # draw second diagonal up
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.4 #draw last line of M
waypoints.append(copy.deepcopy(wpose))

execute_trajectory(move_group,waypoints)
# #calculate cartesian plan
# (plan, fraction) = move_group.compute_cartesian_path(
#     waypoints, 0.01, 0.0
# )

# #execute plan to draw M
# move_group.execute(plan, wait=True)


#Draw J
waypoints = []
wpose = move_group.get_current_pose().pose
#move to start position in top left
wpose.position.x+=.4
wpose.position.y+=.3
waypoints.append(copy.deepcopy(wpose))

#draw first horizontal line
wpose.position.y-=.3
waypoints.append(copy.deepcopy(wpose))

#go back to middle of horizontal line
wpose.position.y+=.15
waypoints.append(copy.deepcopy(wpose))

#draw vertical line down
wpose.position.x-=.3
waypoints.append(copy.deepcopy(wpose))

#draw circle with radius of .1 to represent curve of J
radius = .1
#set the center of the circle, which is to the left of the current pose of the robot
center_y = wpose.position.y + .101 #.101 gets out of a singularity, .1 doesn't work
center_x = wpose.position.x

#iterate through angles and calculate circle position
#circle drawn on the bottom
#negative signs and sines and cosines allow for that
for angle in range (0,180):
    #convert angles to radians and calculate path along circle
    wpose.position.x=-radius*math.sin(angle*3.1415 / 180) + center_x
    wpose.position.y=-radius*math.cos(angle*3.1415 / 180) + center_y
    #append each point along the circule
    waypoints.append(copy.deepcopy(wpose))

#draw J
execute_trajectory(move_group, waypoints)

#reset to bottom left pose for F
reset_to_start_pose(move_group)

waypoints = []
wpose = move_group.get_current_pose().pose

wpose.position.x+=.4 #draw first vertical line of F
waypoints.append(copy.deepcopy(wpose))

wpose.position.y-=.3 #draw first horizontal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.y+=.3 #go back to top left corner
waypoints.append(copy.deepcopy(wpose))

wpose.position.x-=.2 #go down to start of second horizontal line
waypoints.append(copy.deepcopy(wpose))

wpose.position.y-=.3 #draw second horizontal line
waypoints.append(copy.deepcopy(wpose))

#draw F
execute_trajectory(move_group, waypoints)